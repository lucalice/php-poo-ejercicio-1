<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
		<label for="Tamaño">Inserte el tamaño de la moto</label>
		<input type="text" id="tamaño" name="Tamaño"/>	
		<label for="numLlantas">Inserte el número de llandas del carro</label>
		<input type="text" id="numLlantas" name="numLlantas"/>	
		<input type="submit">
	</form>

	<?php

	//crea aqui la clase Moto junto con dos propiedades public
	class Moto{
		//declaracion de propiedades
		public $Tamaño;
		public $numLlantas;
	}
	//crea aqui la instancia o el objeto de la clase Moto
	$Moto1 = new Moto;
	if ( !empty($_POST)){
		$Moto1->Tamaño = $_POST['Tamaño']; //Le asignamos el valor que tengamos en POST al atributo Tamaño de la clase
		$Moto1->numLlantas = $_POST['numLlantas'];
		// recibe aqui los valores mandados por post y arma el mensaje para front 

		echo "El tamaño de la moto es de ";
		echo $Moto1->Tamaño;
		echo " y el número de llantas es ";
		echo $Moto1->numLlantas;
	}  


	?>

</body>
</html>